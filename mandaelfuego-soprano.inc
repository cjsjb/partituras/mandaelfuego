\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key b \minor

		R1*2  |
		r2 r4 fis' 8 e'  |
		fis' 4 fis' 2. (  |
%% 5
		e' 4 ) e' e' d' 8 fis' ~  |
		fis' 8 fis' ~ fis' 4. r8 r4  |
		r2 r4 fis' 8 e'  |
		fis' 4 fis' 2. (  |
		e' 4 ) e' e' d' 8 fis' ~  |
%% 10
		fis' 8 fis' ~ fis' 2 r4  |
		r4 b' 8 b' b' 4 cis''  |
		d'' 4 ( cis'' ) b' ais'  |
		b' 2 b' ~  |
		b' 1  |
%% 15
		r4 b' 8 b' b' 4 cis''  |
		d'' 4 ( cis'' ) b' ais'  |
		ais' 2 ( b' )  |
		ais' 2. r4  |
		b' 2 a' 4 a'  |
%% 20
		b' 4 a' 8 fis' ~ fis' 4 r  |
		b' 4 b' a' 8 a' 4 fis' 8 ~  |
		fis' 2. r4  |
		b' 2 a' 4 a'  |
		b' 4 a' 8 fis' ~ fis' 4 r  |
%% 25
		b' 4 b' a' 8 a' 4 fis' 8 ~  |
		fis' 2. r4  |
		b' 4 b' a' 8 a' 4 b' 8 ( ~  |
		b' 8 a' 4 fis' 4. ) r4  |
		b' 4 b' a' 8 a' 4 fis' 8 ~  |
%% 30
		fis' 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		Man -- "da el" fue -- go __ des -- de lo al -- to. __
		Man -- "da el" fue -- go __ des -- de lo al -- to. __

		Man -- "da el" fue -- go des -- de lo al -- to. __
		Man -- "da el" fue -- go des -- de lo al -- to.

		Ven, San -- "to Es" -- pí -- ri tu, __ ven con tu po -- der. __
		Ven, San -- "to Es" -- pí -- ri tu, __ ven con tu po -- der. __
		Ven con tu po -- der, __ ven con tu po -- der. __
	}
>>
