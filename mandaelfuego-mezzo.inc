\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key b \minor

		R1*2  |
		r4 b 8 a b 4 b ( ~  |
		b 2 d'  |
%% 5
		cis' 4 ) cis' cis' b 8 d' ~  |
		d' 8 d' ~ d' 4. r8 r4  |
		r4 b 8 a b 4 b ( ~  |
		b 2 d'  |
		cis' 4 ) cis' cis' b 8 d' ~  |
%% 10
		d' 8 d' ~ d' 2 r4  |
		r4 g' 8 g' g' 4 a'  |
		b' 4 ( a' ) g' fis'  |
		d' 2 d' ~  |
		d' 1  |
%% 15
		r4 g' 8 g' g' 4 a'  |
		b' 4 ( a' ) g' fis'  |
		fis' 2 ( g' )  |
		fis' 2. r4  |
		g' 2 e' 4 e'  |
%% 20
		fis' 4 e' 8 d' ~ d' 4 r  |
		g' 4 g' e' 8 e' 4 d' 8 ~  |
		d' 2. r4  |
		g' 2 e' 4 e'  |
		fis' 4 e' 8 d' ~ d' 4 r  |
%% 25
		g' 4 g' e' 8 e' 4 d' 8 ~  |
		d' 2. r4  |
		g' 4 g' e' 8 e' 4 fis' 8 ( ~  |
		fis' 8 e' 4 d' 4. ) r4  |
		g' 4 g' e' 8 e' 4 d' 8 ~  |
%% 30
		d' 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzo" {
		Man -- "da el" fue -- go __ des -- de lo al -- to. __
		Man -- "da el" fue -- go __ des -- de lo al -- to. __

		Man -- "da el" fue -- go des -- de lo al -- to. __
		Man -- "da el" fue -- go des -- de lo al -- to.

		Ven, San -- "to Es" -- pí -- ri tu, __ ven con tu po -- der. __
		Ven, San -- "to Es" -- pí -- ri tu, __ ven con tu po -- der. __
		Ven con tu po -- der, __ ven con tu po -- der. __
	}
>>
