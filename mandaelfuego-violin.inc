\context Staff = "Violin" <<
	\set Staff.instrumentName = "Violín"
	\set Staff.shortInstrumentName = "Vln."
	\set Staff.midiInstrument = "Violin"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-violin" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key b \minor

		fis' 1 :16 |
		fis' 1 :16 |
		fis' 1 :16 |
		fis' 1 :16 |
%% 5
		e' 1 :16 |
		fis' 2. r4  |
		fis' 1 :16 |
		fis' 1 :16 |
		e' 1 :16 |
%% 10
		fis' 2. r4  |
		r4 b' 2 cis'' 4  |
		d'' 4 cis'' b' ais'  |
		b' 2 b'  |
		b' 4 a' g' fis'  |
%% 15
		g' 1  |
		r4 e' e' g'  |
		fis' 2 g'  |
		fis' 2. r4  |
		b' 2 a' 4 a'  |
%% 20
		b' 4 a' 8 fis' ~ fis' 4 r  |
		b' 4 b' a' 8 a' 4 fis' 8 ~  |
		fis' 2. r4  |
		g' 2 e' 4 e'  |
		fis' 4 e' 8 d' ~ d' 4 r  |
%% 25
		g' 4 g' e' 8 e' 4 d' 8 ~  |
		d' 2. r4  |
		g' 4 g' e' 8 e' 4 fis' 8 ( ~  |
		fis' 8 e' 4 ) d' 4. r4  |
		d' 4 d' cis' 8 cis' 4 b 8 ~  |
%% 30
		b 1  |
		R1  |
		\bar "|."
	}
>>
