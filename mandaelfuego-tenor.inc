\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble_8"
		\key b \minor

		R1  |
		r2 r4 fis 8 e  |
		fis 4 fis 2. ( ~  |
		fis 1  |
%% 5
		e 4 ) e e d 8 fis ~  |
		fis 8 fis ~ fis 4. r8 fis e  |
		fis 4 fis 2. ( ~  |
		fis 1  |
		e 4 ) e e d 8 fis ~  |
%% 10
		fis 8 fis ~ fis 2 r4  |
		r4 b 8 b b 4 cis'  |
		d' 4 ( cis' ) b ais  |
		b 2 b ~  |
		b 1  |
%% 15
		r4 b 8 b b 4 cis'  |
		d' 4 ( cis' ) b ais  |
		ais 2 ( b )  |
		cis' 2. r4  |
		d' 2 cis' 4 cis'  |
%% 20
		d' 4 cis' 8 b ~ b 4 r  |
		d' 4 d' cis' 8 cis' 4 b 8 ~  |
		b 2. r4  |
		d' 2 cis' 4 cis'  |
		d' 4 cis' 8 b ~ b 4 r  |
%% 25
		d' 4 d' cis' 8 cis' 4 b 8 ~  |
		b 2. r4  |
		d' 4 d' cis' 8 cis' 4 d' 8 ( ~  |
		d' 8 cis' 4 b 4. ) r4  |
		d' 4 d' cis' 8 cis' 4 b 8 ~  |
%% 30
		b 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		Man -- "da el" fue -- go __ des -- de lo al -- to. __
		Man -- "da el" fue -- go __ des -- de lo al -- to. __

		Man -- "da el" fue -- go des -- de lo al -- to. __
		Man -- "da el" fue -- go des -- de lo al -- to.

		Ven, San -- "to Es" -- pí -- ri tu, __ ven con tu po -- der. __
		Ven, San -- "to Es" -- pí -- ri tu, __ ven con tu po -- der. __
		Ven con tu po -- der, __ ven con tu po -- der. __
	}
>>
